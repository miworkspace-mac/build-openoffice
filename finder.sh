#!/bin/bash

NEWLOC=`curl -L "https://sourceforge.net/projects/openofficeorg.mirror/files/latest/download" -A "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36" 2>/dev/null | grep -i .dmg | head -1 | awk -F 'url=' '{print $2}' | sed 's@">@@g'`

if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi

# The above works when sourceforge uploads their latest download link

#NEWLOC=`curl -L "https://snootles.dsc.umich.edu/packages/openoffice/" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' 2>/dev/null | /usr/local/bin/htmlq -a href a | grep .dmg | tail -1 | sed 's@\.\/@@'`

#if [ "x${NEWLOC}" != "x" ]; then
#	echo "https://snootles.dsc.umich.edu/packages/openoffice/${NEWLOC}"
#fi