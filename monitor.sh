#!/bin/bash -ex

LINK=`./finder.sh`

URL=`curl -L -I ${LINK} 2>/dev/null | grep -i '^ETag' | sed 's/ETag\: //i;' | tr -d '\r'`

if [ "x${URL}" != "x" ]; then
    echo URL: "${URL}"
    echo "${URL}" > current-url
fi

# Update to handle distributed builds
if cmp current-url old-url; then
    # Files are identical, exit 1 to NOT trigger the build job
    exit 1
else
    # Files are different - copy marker, exit 0 to trigger build job
    cp current-url old-url
    exit 0
fi
